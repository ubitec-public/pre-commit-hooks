#!/usr/bin/env bash

set -e

rej_files_count=$(find . -type f -name "*.rej" | wc -l)

if [ $rej_files_count -eq 1 ]; then
    echo "There is $rej_files_count rejection file in the project"
elif [ $rej_files_count -gt 0 ]; then
    echo "There are $rej_files_count rejection files in the project"
else
    exit 0
fi

find . -type f -name "*.rej" | while read file; do
    echo $file
done

exit 1